package by.mitrofan.internal.testProject.webmvc;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;

@RestController
@RequestMapping(path = "/app", produces = MediaType.APPLICATION_JSON_VALUE)
public class AppController {
  @GetMapping
  public AppConfig appConfig(ServletRequest request) {
    String contextPath = request.getServletContext().getContextPath();

    if (StringUtils.isEmpty(contextPath)) {
      return new AppConfig("/");
    }

    return new AppConfig(contextPath);
  }

  @RequiredArgsConstructor
  @Getter
  public static final class AppConfig {
    private final String contextPath;
  }
}
