package by.mitrofan.internal.testProject.webmvc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan
public class ApiConfig {
  public static final String URI_PREFIX = "/api";
}
