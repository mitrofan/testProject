package by.mitrofan.internal.testProject;

import by.mitrofan.internal.testProject.root.AppConfig;
import by.mitrofan.internal.testProject.webmvc.ApiConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[] {AppConfig.class};
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[] {ApiConfig.class};
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] {ApiConfig.URI_PREFIX + "/*"};
  }
}
