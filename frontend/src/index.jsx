import 'typeface-roboto';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppContainer from './AppContainer';
import configureStore from './utils/configureStore';
import './index.css';

const containerElement = document.createElement('div');
containerElement.style.height = '100%';
document.body.appendChild(containerElement);

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  containerElement,
);
