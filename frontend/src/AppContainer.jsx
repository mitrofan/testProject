import { connect } from 'react-redux';
import { fetchApp } from './appSlice';
import App from './App';

const mapStateToProps = (state) => ({
  contextPath: state.app.contextPath,
  loading: state.app.loading,
});

const mapDispatchToProps = {
  fetchApp,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
