import { configureStore } from 'redux-starter-kit';
import appReducer from '../appReducer';

export default function (preloadedState) {
  return configureStore({
    reducer: appReducer,
    preloadedState,
  });
}
