import { createSlice } from 'redux-starter-kit';

const appSlice = createSlice({
  name: 'app',
  initialState: {
    contextPath: '/',
    error: '',
    loading: false,
  },
  /* eslint-disable no-param-reassign */
  reducers: {
    loadAppSuccess: (state, action) => {
      state.contextPath = action.payload.contextPath;
      state.loading = false;
    },
    loadAppFailure: (state, action) => {
      state.error = action.payload.error;
      state.loading = false;
    },
    loadAppStart: (state) => {
      state.error = null;
      state.loading = true;
    },
  },
  /* eslint-enable no-param-reassign */
});

export const { loadAppSuccess, loadAppFailure } = appSlice.actions;

export default appSlice.reducer;

export const fetchApp = () => async (dispatch) => {
  let appResponse;
  dispatch(appSlice.actions.loadAppStart());
  try {
    const appResponseObj = await fetch('api/app', {
      headers: {
        Accept: 'application/json',
      },
    });
    if (!appResponseObj.ok) {
      throw new Error(`Response is: ${appResponseObj.statusText}`);
    }

    appResponse = await appResponseObj.json();
  } catch (err) {
    dispatch(loadAppFailure(err.toString()));
    return;
  }
  dispatch(loadAppSuccess(appResponse));
};
