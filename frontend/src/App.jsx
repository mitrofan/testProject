import React, { lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import LoadingIndicator from './components/LoadingIndicator';

import AppBar from './components/AppBar';

const Counter = lazy(() => import('./features/counter'));
const Empty = lazy(() => import('./features/empty'));
const Progress = lazy(() => import('./features/progress'));

class App extends React.Component {
  componentDidMount() {
    const { fetchApp } = this.props;

    fetchApp();
  }

  render() {
    const { contextPath, loading } = this.props;

    return (
      <Box display="flex" flexDirection="column" height="100%">
        <CssBaseline />

        { (loading && <LoadingIndicator />) || (
          /* eslint-disable-next-line no-undef */
          <Router basename={contextPath}>
            <Box flexGrow="0" flexShrink="1" flexBasis="auto">
              <AppBar />
            </Box>

            <Box flexGrow="1" flexShrink="1" flexBasis="auto">
              <Suspense fallback={<LoadingIndicator />}>
                <Switch>
                  <Route path="/counter">
                    <Counter />
                  </Route>
                  <Route path="/progress">
                    <Progress />
                  </Route>
                  <Route path="/">
                    <Empty />
                  </Route>
                </Switch>
              </Suspense>
            </Box>
          </Router>
        ) }
      </Box>

    );
  }
}

App.propTypes = {
  fetchApp: PropTypes.func.isRequired,
  contextPath: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default App;
