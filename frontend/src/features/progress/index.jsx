import React from 'react';
import LoadingIndicator from '../../components/LoadingIndicator';

export default () => (
  <LoadingIndicator />
);
