import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3, 2),
  },
}));

const Counter = ({ counter, increment }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Typography component="p">
        {counter}
      </Typography>
      <Button variant="contained" color="primary" onClick={() => increment()}>
        Increment
      </Button>
    </Paper>
  );
};

Counter.propTypes = {
  counter: PropTypes.number.isRequired,
  increment: PropTypes.func.isRequired,
};

export default Counter;
