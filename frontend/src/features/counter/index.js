import { connect } from 'react-redux';
import { increment } from './counterSlice';
import Counter from './components/Counter';

const mapStateToProps = (state) => ({ counter: state.features.counter });
const mapDispatchToProps = { increment };

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
