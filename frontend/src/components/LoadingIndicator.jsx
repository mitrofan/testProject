import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';

export default () => (
  <Box display="flex" alignItems="center" justifyContent="center" height="100%">
    <CircularProgress />
  </Box>
);
