import React from 'react';
import List from '@material-ui/core/List';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import AutorenewRoundedIcon from '@material-ui/icons/AutorenewRounded';

import RoutingListItem from './RoutingListItem';

export default () => (
  <List>
    <RoutingListItem text="Home" icon={<InboxIcon />} link="/" />
    <RoutingListItem text="Counter" icon={<MailIcon />} link="/counter" />
    <RoutingListItem text="Progress" icon={<AutorenewRoundedIcon />} link="/progress" />
  </List>
);
