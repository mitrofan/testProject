import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link as RouterLink } from 'react-router-dom';

export default function RoutingListItem({ text, icon, link }) {
  const renderLink = React.useMemo(
    () => React.forwardRef((itemProps, ref) => (
      // eslint-disable-next-line react/jsx-props-no-spreading
      <RouterLink to={link} {...itemProps} innerRef={ref} />
    )),
    [link],
  );

  return (
    <ListItem button component={renderLink}>
      <ListItemIcon>
        {icon}
      </ListItemIcon>
      <ListItemText primary={text} />
    </ListItem>
  );
}

RoutingListItem.propTypes = {
  text: PropTypes.string.isRequired,
  icon: PropTypes.element.isRequired,
  link: PropTypes.string.isRequired,
};
