import { combineReducers } from 'redux';
import featuresReducer from './features/featuresReducer';
import appReducer from './appSlice';

export default combineReducers({
  features: featuresReducer,
  app: appReducer,
});
