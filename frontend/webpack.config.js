const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');
const apiMocker = require('connect-api-mocker');

module.exports = {
  output: {
    chunkFilename: '[name].[contenthash].js',
  },
  mode: 'development',
  devtool: 'inline-source-map',
  resolve: {
    extensions: [
      '.js',
      '.jsx',
    ],
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader',
        ],
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            // options are externalized to .babelrc for sharing with eslint
          },
          {
            loader: 'eslint-loader',
            options: {
              failOnError: true,
              failOnWarning: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin(),
  ],
  devServer: {
    contentBase: false,
    historyApiFallback: true,
    before(app) {
      app.use(apiMocker('/api', 'mocks/api'));
    },
  },
};
